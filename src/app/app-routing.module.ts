import { NgModule } from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {AuthorDetailComponent} from './components/author-detail/author-detail.component';
import {AuthorListComponent} from './components/author-list/author-list.component';
import {BookListComponent} from './components/book-list/book-list.component';
import {BookDetailComponent} from './components/book-detail/book-detail.component';

const routes: Routes = [
  { path: 'authors', component: AuthorListComponent },
  { path: 'author/:id', component: AuthorDetailComponent },
  { path: 'books', component: BookListComponent },
  { path: 'book/:id', component: BookDetailComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
