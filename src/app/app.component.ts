import {Component, OnInit} from '@angular/core';
import { ROUTES } from './mock/routes';
import {Routes} from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  title = 'book author frontend';
  showNav = false;
  routes: Routes;

  ngOnInit() {
    this.routes = ROUTES;
  }

  public toggleNav(): void {
    this.showNav = !this.showNav;
  }
}
