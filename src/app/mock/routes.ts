import {Route} from '@angular/router';
import {AuthorListComponent} from '../components/author-list/author-list.component';
import {BookListComponent} from '../components/book-list/book-list.component';

export const ROUTES: Route[] = [
  { path: 'authors', component: AuthorListComponent },
  { path: 'books', component: BookListComponent },
];
