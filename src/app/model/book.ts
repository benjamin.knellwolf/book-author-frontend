export interface Book {
  id?: bigint;
  title?: string;
  description?: string;
  price?: bigint;
}
