import {Book} from './book';

export interface Author {
  id?: number;
  firstName?: string;
  lastName?: string;
  balance?: number;
  birthDate?: Date;
  books?: Book[];
}
