import {AfterViewInit, Component, OnInit, ViewChild} from '@angular/core';
import {Author} from '../../model/author';
import {AuthorService} from '../../service/author.service';
import {Observable, of} from 'rxjs';
import {MatTable, MatTableDataSource} from '@angular/material/table';
import {MatPaginator} from '@angular/material/paginator';

@Component({
  selector: 'app-author-list',
  templateUrl: './author-list.component.html',
  styleUrls: ['./author-list.component.css']
})
export class AuthorListComponent implements OnInit, AfterViewInit {

  public dataSource: MatTableDataSource<Author>;
  public displayedColumns: string[] = ['id', 'firstName', 'lastName', 'balance', 'edit', 'delete'];
  @ViewChild(MatPaginator)
  public paginator: MatPaginator;

  constructor(
    private authorService: AuthorService
  ) { }

  ngOnInit(): void {
    this.authorService
      .getAuthors()
      .subscribe(authors => {
        this.dataSource = new MatTableDataSource<Author>(authors);
      });
  }

  ngAfterViewInit(): void {
    setTimeout(() => {
      this.dataSource.paginator = this.paginator;
    }, 200);
  }

  public deleteAuthor(author: Author): void {
    this.authorService.deleteAuthorById(author.id);
    window.location.reload();
  }
}
