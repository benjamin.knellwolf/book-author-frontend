import { Component, OnInit } from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {AuthorService} from '../../service/author.service';
import {Author} from '../../model/author';

@Component({
  selector: 'app-author-detail',
  templateUrl: './author-detail.component.html',
  styleUrls: ['./author-detail.component.css']
})
export class AuthorDetailComponent implements OnInit {

  public author: Author;

  constructor(
    private activatedRoute: ActivatedRoute,
    private authorService: AuthorService
  ) { }

  ngOnInit(): void {
    this.authorService
      .getAuthorById(Number(this.activatedRoute.snapshot.paramMap.get('id')))
      .subscribe(author => this.author = author);
  }

  public saveAuthor(author: Author): void {
    this.authorService
      .saveAuthor(author)
      .subscribe(response => {
        console.log(response);
      });
  }

}
