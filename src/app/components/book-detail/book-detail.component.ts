import { Component, OnInit } from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {Book} from '../../model/book';
import {BookService} from '../../service/book.service';

@Component({
  selector: 'app-book-detail',
  templateUrl: './book-detail.component.html',
  styleUrls: ['./book-detail.component.css']
})
export class BookDetailComponent implements OnInit {

  public book: Book;

  constructor(
    private activatedRoute: ActivatedRoute,
    private bookService: BookService
  ) { }

  ngOnInit(): void {
    this.bookService
      .getBookById(Number(this.activatedRoute.snapshot.paramMap.get('id')))
      .subscribe(book => this.book = book);
  }

  public saveBook(book: Book): void {
    this.bookService
      .saveBook(book)
      .subscribe(response => {
        console.log(response);
      });
  }

}
