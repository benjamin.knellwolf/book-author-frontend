import {AfterViewInit, Component, OnInit, ViewChild} from '@angular/core';
import {MatTableDataSource} from '@angular/material/table';
import {MatPaginator} from '@angular/material/paginator';
import {BookService} from '../../service/book.service';
import {Book} from '../../model/book';

@Component({
  selector: 'app-book-list',
  templateUrl: './book-list.component.html',
  styleUrls: ['./book-list.component.css']
})
export class BookListComponent implements OnInit, AfterViewInit {
  public dataSource: MatTableDataSource<Book>;
  public displayedColumns: string[] = ['id', 'title', 'price', 'edit', 'delete'];
  @ViewChild(MatPaginator)
  public paginator: MatPaginator;

  constructor(
    private bookService: BookService
  ) { }

  ngOnInit(): void {
    this.bookService
      .getBooks()
      .subscribe(books => {
        this.dataSource = new MatTableDataSource<Book>(books);
      });
  }

  ngAfterViewInit(): void {
    setTimeout(() => {
      this.dataSource.paginator = this.paginator;
    }, 200);
  }

  public deleteBook(book: Book): void {
    this.bookService.deleteBookById(book.id);
    window.location.reload();
  }
}
