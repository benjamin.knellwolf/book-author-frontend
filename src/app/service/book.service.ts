import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {Book} from '../model/book';

@Injectable({
  providedIn: 'root'
})
export class BookService {

  private API = 'http://localhost:8080/api/book/';

  constructor(
    private httpClient: HttpClient
  ) { }

  public getBooks(): Observable<Book[]> {
    return this.httpClient.get<Book[]>(this.API);
  }

  public getBookById(id: number): Observable<Book> {
    return this.httpClient.get<Book>(this.API + id);
  }

  public saveBook(book: Book): Observable<Book> {
    return this.httpClient.post(this.API, book);
  }

  public deleteBookById(id: bigint): void {
    this.httpClient.delete(this.API + id)
      .subscribe();
  }
}
