import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable, of} from 'rxjs';
import {Author} from '../model/author';

@Injectable({
  providedIn: 'root'
})
export class AuthorService {

  private API = 'http://localhost:8080/api/author/';

  constructor(
    private httpClient: HttpClient
  ) { }

  public getAuthors(): Observable<Author[]> {
    return this.httpClient.get<Author[]>(this.API);
  }

  public getAuthorById(id: number): Observable<Author> {
    return this.httpClient.get<Author>(this.API + id);
  }

  public saveAuthor(author: Author): Observable<Author> {
    return this.httpClient.post<Author>(this.API, author);
  }

  public deleteAuthorById(id: number): void {
    this.httpClient.delete(this.API + id)
      .subscribe();
  }
}
